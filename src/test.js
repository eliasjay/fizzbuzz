let texto = (t, c, x) => {
    let tx = {
        0: txt(),
        1: txc(x),
        2: txc(x)
    }

    return tx[t][c]
}

let txt = () => {
    let tx = {
        0: 'fizzbuzz',
        1: 'fizz',
        2: 'fizz',
        3: 'fizz',
        4: 'fizz'
    }
    return tx
}

let txc = (x) => {
    let tx = {
        0: 'buzz',
        1: x,
        2: x,
        3: x,
        4: x
    }
    return tx
}

for (let x = 1; x <= 100; x++) {
    let d3 = x % 3;
    let d5 = x % 5;

    console.log(texto(d3, d5, x));
}