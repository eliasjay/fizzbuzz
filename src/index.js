let fbStructur = {
    0: (fbNumber) => (fbNumber),
    1: (fbNumber) => 'fizz',
    2: (fbNumber) => 'buzz',
    3: (fbNumber) => 'fizzbuzz'
}

exports.multOfThree = (number) => {
    return number % 3 == 0
}
exports.multOfFive = (number) => {
    return number % 5 == 0
}

exports.booleanToDecimal = (number) => {
    let n1 = exports.multOfThree(number);
    let n2 = exports.multOfFive(number);
    return Number(n2) * 2 + Number(n1)
}

exports.printer = (number) => {
    return fbStructur[exports.booleanToDecimal(number)]
}

for (let i = 1; i <= 100; i++) {
    console.log(exports.booleanToDecimal(i), i)
}